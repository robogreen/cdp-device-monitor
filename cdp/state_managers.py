"""Classes and utilities to interprit the states of the device"""

import logging

from typing import List, Optional, Type
from cdp.states import BatteryState, PowerState, State, Transition
from cdp.packets import Packet


logger = logging.getLogger(__name__)


class InvalidTransitionError(Exception):
    """Invalid state transition detected."""

    pass


class StateNotFoundError(Exception):
    """No valid state found."""

    pass


class NoInitialStateError(Exception):
    """No valid starting state provided"""

    pass


class StateManager:
    """Base class definition for state managers, these are designed to
    interpret the current state of the device components when supplied
    with respective data packets.
    """

    def __init__(self, initial_state: Optional[Type[State]] = None, time_ms: int = 0):
        self.time_ms = time_ms

        # if there is no initial state, use state where id=0, else raise
        if not initial_state:
            initial_state = self._get_state_by_id(0)

        if not initial_state:
            raise NoInitialStateError

        self.__state: Type[State] = initial_state
        # self.state = starting_state

    # No packets at init
    current_packet: Optional[Type[Packet]] = None

    @property
    def state(self) -> Type[State]:
        return self.__state

    @state.setter
    def state(self, new_state: Type[State]) -> None:
        """Set state, checks valid transition"""
        # if no state, transition from nothing to first state is valid
        # but is not logged as state change.
        transition = Transition(self.state, new_state)
        if self.__state is None:
            self.__state = new_state
        elif self._is_transition_valid(transition):
            self.__state = new_state
            self._log_transition(transition)
        else:
            raise InvalidTransitionError

    @property
    def time_s(self) -> int:
        return self._ms_to_s(self.time_ms)

    def _ms_to_s(self, time_ms: int) -> int:
        seconds = (time_ms / 1000.0) // 1
        return int(seconds)

    @property
    def valid_states(self) -> List[Type[State]]:
        """Valid states definition, order by id ascending"""
        raise NotImplementedError("Valid states must be implemented by subclass.")

    @property
    def valid_transitions(self) -> List[Transition]:
        """Valid transitions definition"""
        raise NotImplementedError("Valid transitions must be implemented by subclass.")

    def _get_state_by_id(self, id: int) -> Optional[Type[State]]:
        """Find valid state by id"""
        result = None
        for state in self.valid_states:
            if id == state.id:
                result = state
        return result

    def _get_state_from_packet(self, packet: Type[Packet]) -> Optional[Type[State]]:
        """Helper method to provide state of single packet"""
        raise NotImplementedError("Must be implemented in subclass.")

    def _is_transition_valid(self, transition: Transition) -> bool:
        """Helper method to provide validity of transition"""
        raise NotImplementedError("Must be implemented in subclass.")

    def _log_transition(self, transition: Transition) -> None:
        """Method to log transition to output, implement in subclass."""
        raise NotImplementedError("Must be implemented in subclass.")

    def new_packet(self, packet: Type[Packet]) -> None:
        """Handler for new packet"""
        raise NotImplementedError("Must be implemented in subclass")


class PowerStateManager(StateManager):
    """State manager for power states"""

    _time_thresh_crossed_ms: int = 0
    transition_time_check_ms: int = 10

    # No pending state changes at init.
    _pending_state: Optional[State] = None

    @property
    def _time_since_thresh_crossed_ms(self) -> int:
        """Definition for time since the threshold was crossed."""
        return self.time_ms - self._time_thresh_crossed_ms

    @property
    def valid_states(self):
        return [
            PowerState(id=0, min_power_mw=0, max_power_mw=200),
            PowerState(id=1, min_power_mw=300, max_power_mw=450),
            PowerState(id=2, min_power_mw=550, max_power_mw=650),
            PowerState(id=3, min_power_mw=800, max_power_mw=1200),
        ]

    @property
    def valid_transitions(self) -> List[Transition]:
        return [
            Transition(self.valid_states[0], self.valid_states[1], name="Starting"),
            Transition(self.valid_states[1], self.valid_states[2], name="Warm up"),
            Transition(self.valid_states[2], self.valid_states[3], name="Main Session"),
            Transition(self.valid_states[3], self.valid_states[2], name="Cool down"),
            Transition(self.valid_states[2], self.valid_states[0], name="Complete"),
        ]

    def _get_state_from_packet(self, packet):
        for state in self.valid_states:
            if (
                packet.power_mw >= state.min_power_mw
                and packet.power_mw <= state.max_power_mw
            ):
                return state

        # if no valid state found, raise not found
        raise StateNotFoundError(
            "No valid state found for power: {}mW".format(packet.power_mw)
        )

    def _is_transition_valid(self, transition: Transition) -> bool:
        return transition in self.valid_transitions

    def _transition_time_check(self) -> bool:
        """Check that the elapsed time sicnce threshold was crossed."""
        return self._time_since_thresh_crossed_ms >= self.transition_time_check_ms

    def _log_transition(self, transition: Transition) -> None:
        logger.info(
            "S;{};{}-{}".format(
                # use time threshold crossed, not current time
                self._ms_to_s(self._time_thresh_crossed_ms),
                transition.from_state.id,
                transition.to_state.id,
            )
        )

    def new_packet(self, packet) -> None:
        """Add new packet, check for state change"""

        # update time to latest packet time
        self.time_ms = packet.time_ms
        self.current_packet = packet
        try:
            packet_state = self._get_state_from_packet(packet)
            # check whether packet state is equal to the current state
            if packet_state == self.state:
                # No pending state change
                self._pending_state = None
            # check whether packet state is equal to pending state change

            elif packet_state == self._pending_state:
                # check for time elapsed since thresh change
                if self._transition_time_check():
                    # perform state change
                    self.state = packet_state
                    self._pending_state = None
            # otherwise, new pending state change
            else:
                self._pending_state = packet_state
                self._time_thresh_crossed_ms = packet.time_ms

        except StateNotFoundError:
            # if no matching state is found, there is no pending state
            # cancel pending state
            logger.debug("No state found for packet: {}".format(packet))
            self._pending_state = None


class BatteryStateManager(StateManager):
    """State manager for battery state management"""

    @property
    def valid_states(self):
        return [
            BatteryState(id=0, name="VLOW"),
            BatteryState(id=1, name="LOW"),
            BatteryState(id=2, name="MED"),
            BatteryState(id=3, name="HIGH"),
        ]

    # no specification provided on valid battery transitions
    valid_transitions: List[Transition] = []

    def _get_state_from_packet(self, packet):
        """Helper method to provide state of single Battery Packet"""

        state = self._get_state_by_id(packet.batteryState)

        if not state:
            # if no state match found, raise error
            raise StateNotFoundError(
                "No BatteryState found for state id: {}".format(packet.batteryState)
            )

        return state

    def _is_transition_valid(self, transition):
        """Helper method to provide validity of transition"""
        # all transitions are valid
        return True

    def _log_transition(self, transition):
        logger.info("B;{};{}".format(self.time_s, transition.to_state.name))

    def new_packet(self, packet):
        """Set new battery state to state provided in packet"""
        # update time to latest packet time
        self.time_ms = packet.time_ms
        self.current_packet = packet

        try:
            packet_state = self._get_state_from_packet(packet)
            self.state = packet_state

        except StateNotFoundError as e:
            # if no matching state is found,
            logger.error("Err:No state found for packet: {}".format(packet))
            raise e
