"""Definitions for device packet types"""


class PacketLengthError(Exception):
    """Unexpected length of packet data."""

    pass


class PacketCheckSumError(Exception):
    """Packet checksum does not match packet error check value"""

    pass


class Packet:
    """Base class definition for device packets"""

    def __init__(self, data: bytes):
        self.data = data

        # check size of packet
        if not self._packet_length_valid():
            raise PacketLengthError

        # convert time from 4 bytes to unint32 to represent milliseconds
        self.time_ms = self._bytes_to_int(self.data[0:4])
        # error check is final byte, regardless of packet size
        # note that indexing a single value from the byte array
        # uses default behaviour, here we explicit in our conversion.
        self.errorCheck = self._bytes_to_int(self.data[-1:-2:-1])

        if self._has_error():
            raise PacketCheckSumError

    _check_sum_mod: int = 256

    def _has_error(self):
        """provides error check where check sum is mod 256 of
        packet data (packet type inclusive)
        """
        # does not include error check value, does include packet type
        bytes_to_sum = self.packet_type.to_bytes(1, "big") + self.data[:-1]
        check = sum(bytes_to_sum) % self._check_sum_mod
        return check != self.errorCheck

    @property
    def length_bytes(self):
        """Length in bytes of packet, packet type not included"""
        raise NotImplementedError("Subclasses must implement length_bytes property.")

    @property
    def packet_type(self):
        """Packet type defintion"""
        raise NotImplementedError("Subclasses must implement packet_type property")

    def _packet_length_valid(self):
        """Checks validity of packet length"""
        valid = True
        if len(self.data) != self.length_bytes:
            valid = False
        return valid

    # requirement: `Numbers from the input shall be network byte ordered.`
    def _bytes_to_int(self, bytes: bytes):
        """Convert bytes to unsigned integer."""
        value = int.from_bytes(bytes, byteorder="big", signed=False)
        return value


class PowerPacket(Packet):
    """Class to represent the format of the power packet"""

    def __init__(self, data: bytes):
        super().__init__(data)
        self.voltage_v = self._bytes_to_int(self.data[4:8])
        self.current_ma = self._bytes_to_int(self.data[8:16])

    length_bytes = 17
    packet_type = 0

    @property
    def power_mw(self) -> int:
        # Power(mW) = Volt (V) x Current (mA)
        return self.voltage_v * self.current_ma


class BatteryPacket(Packet):
    """Class to represent the battery information packet"""

    def __init__(self, data: bytes):
        super().__init__(data)
        self.batteryState = self._bytes_to_int(self.data[4:5])

    length_bytes = 6
    packet_type = 1
