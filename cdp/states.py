"""Contains State and Transitions definitions and utlities"""


from typing import Type


class State:
    """Base class for device States"""

    def __init__(self, id: int, name="") -> None:
        self.id = id
        self.name = name

    def __eq__(self, other: object) -> bool:
        raise NotImplementedError("__eq__ must be defined in subclass.")


class PowerState(State):
    """Power state, includes voltage thresholds"""

    def __init__(self, id: int, min_power_mw: int, max_power_mw: int, name="") -> None:
        super().__init__(id, name=name)
        self.min_power_mw = min_power_mw
        self.max_power_mw = max_power_mw

    def __eq__(self, other: object) -> bool:

        if not isinstance(other, PowerState):
            return False

        return all(
            [
                self.id == other.id,
                self.min_power_mw == other.min_power_mw,
                self.max_power_mw == other.max_power_mw,
            ]
        )


class BatteryState(State):
    """Battery state, requires battery level name"""

    def __init__(self, id: int, name: str) -> None:
        super().__init__(id, name=name)

    def __eq__(self, other: object) -> bool:

        if not isinstance(other, BatteryState):
            return False

        return all([self.id == other.id, self.name == other.name])


class Transition:
    """Base class definiton for state transitions"""

    def __init__(
        self,
        from_state: Type[State],
        to_state: Type[State],
        name="",
    ) -> None:
        self.from_state = from_state
        self.to_state = to_state
        self.name = name

    def __eq__(self, other: object) -> bool:

        if not isinstance(other, Transition):
            return False

        # We do not require the optional name for equality
        return all(
            [self.from_state == other.from_state, self.to_state == other.to_state]
        )
