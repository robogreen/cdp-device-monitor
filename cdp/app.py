"""Classes and utlities for the core application"""

import logging

from cdp.packets import BatteryPacket, PacketCheckSumError, PowerPacket
from cdp.state_managers import (
    BatteryStateManager,
    InvalidTransitionError,
    PowerStateManager,
)
from cdp.file_reader import ByteFileReader, UnknownPacketType


logger = logging.getLogger(__name__)


class App:
    def __init__(self, file_reader: ByteFileReader) -> None:
        self.file_reader = file_reader

    powerManager = PowerStateManager()

    batteryManager = BatteryStateManager()

    def run(self) -> None:
        """Run the application"""

        # check for end of file reached
        while not self.file_reader.end_of_file:

            try:
                next_packet = self.file_reader.read_packet()

                if isinstance(next_packet, PowerPacket):
                    self.powerManager.new_packet(next_packet)

                elif isinstance(next_packet, BatteryPacket):
                    self.batteryManager.new_packet(next_packet)

            except UnknownPacketType as e:
                logger.error("ERR:Unknown Packet Type")
                # without knowing the unknown packet size, there
                # isn't a gaceful way to fail.
                raise e
            except PacketCheckSumError:
                logger.error("ERR: Packet Check Error")
                # continue program
            except InvalidTransitionError:
                logger.error("ERR: Illegal Transition")
