import argparse
import logging

from cdp.file_reader import ByteFileReader
from cdp.app import App


parser = argparse.ArgumentParser(
    description="Monitor input file for state changes detected in device data packets"
)
parser.add_argument(
    "input_file",
    type=str,
    help="input file containing packet data in binary form",
)

LOGGER_FORMAT = "%(message)s"
LOGGER_LEVEL = logging.INFO


def main():
    """Application entry point"""

    logging.basicConfig(format=LOGGER_FORMAT, level=LOGGER_LEVEL)
    args = parser.parse_args()
    file_reader = ByteFileReader(args.input_file, cursor=0)
    app = App(file_reader)
    app.run()


if __name__ == "__main__":
    main()
