"""Module for reading the byte array device files"""

import os
from typing import List, Type

from cdp.packets import BatteryPacket, Packet, PowerPacket
from pathlib import Path


class InvalidFileType(Exception):
    """Invalid file type given."""

    pass


class UnknownPacketType(Exception):
    """The packet type was not recognised."""

    pass


class EndOfFileReached(Exception):
    """The end of the file has been reached"""

    pass


class ByteFileReader:
    def __init__(self, file, cursor=0):
        self.file_path = Path(file)
        self.cursor = cursor

        if not self._fileDoesExist():
            raise FileNotFoundError("No file found for path: {}".format(file))

        if not self._isValidFileType():
            raise InvalidFileType(
                "Invalid file type given ({}), expect '.bin'".format(file)
            )

    end_of_file = False
    packet_types: List[Type[Packet]] = [PowerPacket, BatteryPacket]

    @property
    def file_size(self):
        return os.path.getsize(self.file_path)

    def _isValidFileType(self) -> bool:
        """Check that the file is a valid .bin file"""
        isValid = False
        ext = self.file_path.suffix
        if ext == ".bin":
            isValid = True
        return isValid

    def _fileDoesExist(self) -> bool:
        """Check that the file exists at provided dir."""
        return self.file_path.is_file()

    def _read_next_bytes(self, bytesToRead=1) -> bytes:
        """Generator to read next byte of file, retain cursor posiiton"""
        # Open in read only, binary mode.
        if self.end_of_file:
            raise EndOfFileReached(
                "End of file: {} reached, no more bytes to read.".format(self.file_path)
            )

        with open(self.file_path, "rb") as file:
            # navigate to cursor position
            file.seek(self.cursor)
            content = file.read(bytesToRead)
            # update cursor with newly read byte count
            self.cursor += bytesToRead
            # check for end of file
            if self.cursor >= self.file_size:
                self.end_of_file = True

            return content

    def read_packet(self) -> Packet:
        """Read the next packet, return packet obj."""

        packet = None
        packet_type = int.from_bytes(
            self._read_next_bytes(1), byteorder="big", signed=False
        )
        for packetDef in self.packet_types:
            if packet_type == packetDef.packet_type:
                bytes_to_read = packetDef.length_bytes
                packet = packetDef(self._read_next_bytes(bytes_to_read))
                return packet

        # if no matching packet definition found
        raise UnknownPacketType("Packet type: {} not recognised.".format(packet_type))
