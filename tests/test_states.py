import typing
import pytest

from cdp.states import BatteryState, PowerState, State, Transition


def test_state_base_class():
    test_name = "first state"
    test_id = 0
    first_state = State(id=test_id, name=test_name)
    assert first_state.id == test_id
    assert first_state.name == test_name


def test_state_equality_raises():
    """tests base Sate class enforces __eq__ subclass implementation"""
    first_state = State(id=0, name="first state")
    second_state = State(id=1, name="second state")

    with pytest.raises(NotImplementedError):
        first_state == second_state


@pytest.mark.parametrize(
    "first_state, second_state, expected",
    [
        (
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            True,
        ),
        (
            PowerState(id=0, min_power_mw=0, max_power_mw=100, name="Optional name"),
            PowerState(
                id=0, min_power_mw=0, max_power_mw=100, name="Different optional name"
            ),
            True,
        ),
        (
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            PowerState(id=1, min_power_mw=0, max_power_mw=100),
            False,
        ),
        (
            PowerState(id=0, min_power_mw=1, max_power_mw=100),
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            False,
        ),
        (
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            PowerState(id=0, min_power_mw=0, max_power_mw=200),
            False,
        ),
        (
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            BatteryState(id=0, name="Name"),
            False,
        ),
    ],
)
def test_powerstate_equality(first_state, second_state, expected):
    """Test PowerState __eq__ correctly equates instances"""
    result = first_state == second_state
    assert result == expected


@pytest.mark.parametrize(
    "first_state, second_state, expected",
    [
        (BatteryState(id=0, name="Name"), BatteryState(id=0, name="Name"), True),
        (BatteryState(id=1, name="Name"), BatteryState(id=0, name="Name"), False),
        (
            BatteryState(id=0, name="Name"),
            BatteryState(id=0, name="DifferentName"),
            False,
        ),
        (
            BatteryState(id=1, name="Name"),
            BatteryState(id=0, name="DifferentName"),
            False,
        ),
        (
            BatteryState(id=1, name="Name"),
            PowerState(id=0, min_power_mw=0, max_power_mw=100),
            False,
        ),
    ],
)
def test_battery_state_equality(first_state, second_state, expected):
    """Test BatterySate __eq__ correctly equates instances"""
    result = first_state == second_state
    assert result == expected


@typing.no_type_check
@pytest.mark.parametrize(
    "first_transition, second_transition, expected",
    [
        (
            Transition(
                BatteryState(id=0, name="Name"), BatteryState(id=1, name="Name")
            ),
            Transition(
                BatteryState(id=0, name="Name"), BatteryState(id=1, name="Name")
            ),
            True,
        ),
        (
            Transition(
                BatteryState(id=0, name="Name"), BatteryState(id=2, name="Name")
            ),
            Transition(
                BatteryState(id=0, name="Name"), BatteryState(id=1, name="Name")
            ),
            False,
        ),
        (
            Transition(
                BatteryState(id=0, name="Name"), BatteryState(id=1, name="Name")
            ),
            Transition(
                BatteryState(id=1, name="Name"), BatteryState(id=2, name="Name")
            ),
            False,
        ),
        (
            Transition(
                BatteryState(id=0, name="Name"), BatteryState(id=1, name="Name")
            ),
            None,
            False,
        ),
    ],
)
def test_transition_equality(first_transition, second_transition, expected):
    """Test Transition __eq__ correctly equates instances"""
    result = first_transition == second_transition
    assert result == expected
