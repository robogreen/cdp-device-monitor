import os
import pytest
import logging

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "data")
TEST_INTEGRATION_FILE = os.path.join(TEST_DATA_DIR, "CodingTest.bin")
TEST_INTEGRATION_FILE_SIZE_BYTES = os.path.getsize(TEST_INTEGRATION_FILE)


@pytest.fixture
def test_logger(caplog):
    """fixture to provide access to messages logged during
    test, uses same logging level as app
    """
    with caplog.at_level(level=logging.INFO):
        yield caplog
