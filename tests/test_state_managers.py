from typing import List, Tuple
from cdp.packets import BatteryPacket, PowerPacket
import pytest

from cdp.states import BatteryState, PowerState, State, Transition
from cdp.state_managers import (
    BatteryStateManager,
    InvalidTransitionError,
    NoInitialStateError,
    PowerStateManager,
    StateManager,
    StateNotFoundError,
)


TEST_BATTERY_STATES = [
    BatteryState(id=0, name="zero"),
    BatteryState(id=1, name="one"),
    BatteryState(id=2, name="two"),
    BatteryState(id=3, name="three"),
]

TEST_POWER_STATES = [
    PowerState(id=0, min_power_mw=0, max_power_mw=100),
    PowerState(id=1, min_power_mw=200, max_power_mw=300),
    PowerState(id=2, min_power_mw=400, max_power_mw=500),
    PowerState(id=3, min_power_mw=600, max_power_mw=700),
    PowerState(id=4, min_power_mw=800, max_power_mw=900),
]

TestPowerPacketData = List[Tuple[int, int]]


@pytest.fixture
def state_manager_with_valid():
    """pytest fixture to provide state manager with transition validity implemented"""
    state_manager = StateManager
    state_manager._is_transition_valid = lambda *_: True
    return state_manager(initial_state=State(id=0))


@pytest.fixture
def test_power_state_manager():
    """pytest fixture to provide PowerStateManager with test states"""
    test_ps_manager = PowerStateManager
    test_ps_manager.valid_states = TEST_POWER_STATES
    test_ps_manager.transition_time_check_ms = 10
    return test_ps_manager


# MAYDO improve by making a context handler, mocking
# power property and yielding from pytest fixture
def patch_power_packet_data(data: TestPowerPacketData) -> List[PowerPacket]:
    """helper to provide list of packets, with overriden time and power data"""
    packets: List[PowerPacket] = []
    for item in data:
        packet_class = PowerPacket
        # override class defined power
        packet_class.power_mw = 0  # type: ignore
        packet = packet_class(bytearray([0 for i in range(17)]))
        packet.time_ms = item[1]
        packet.power_mw = item[0]  # type: ignore
        packets.append(packet)
        # add packet to state manager
    return packets


def test_state_manager_sets_initial_state():
    """tests that StateMansger correctly sets the initial state"""
    initial_state = BatteryState(id=3, name="HIGH")
    state_manager = BatteryStateManager(initial_state=initial_state)
    assert state_manager.state == initial_state


def test_state_manager_sets_initial_state_without_arg():
    """tests that StateManager correctly sets initial state by default"""
    state_manager = BatteryStateManager()
    assert state_manager.state == BatteryState(id=0, name="VLOW")


def test_state_manager_no_initial_state_raises():
    """tests that StateManager without valid default states raises"""
    state_manager = StateManager
    state_manager.valid_states = [State(id=1)]
    with pytest.raises(NoInitialStateError):
        state_manager()


@pytest.mark.parametrize(
    "time_ms, time_s", [(1000, 1), (1005, 1), (1999, 1), (2000, 2)]
)
def test_state_manager_time_s(time_ms, time_s, state_manager_with_valid):
    """ "tests time_s property, returns floored integer"""
    state_manager_with_valid.time_ms = time_ms
    assert type(state_manager_with_valid.time_s) == int
    assert state_manager_with_valid.time_s == time_s


@pytest.mark.parametrize(
    "id, state",
    [
        (0, TEST_BATTERY_STATES[0]),
        (1, TEST_BATTERY_STATES[1]),
        (2, TEST_BATTERY_STATES[2]),
        (3, TEST_BATTERY_STATES[3]),
        (4, None),
    ],
)
def test_state_manager_get_state_by_id(id, state):
    """tests get state by id returns correct state"""
    state_manager = BatteryStateManager
    state_manager.valid_states = TEST_BATTERY_STATES
    assert state_manager()._get_state_by_id(id) == state


def test_state_manager_state_setter():
    """tests the setter method for state property"""
    state_manager = PowerStateManager()
    states = state_manager.valid_states
    new_state = states[1]
    state_manager.state = new_state
    assert state_manager.state == new_state


def test_state_manager_state_setter_raises():
    """tests that an error is raised on invalid transitions"""
    state_manager = PowerStateManager()
    states = state_manager.valid_states
    new_state = states[3]
    assert (
        Transition(state_manager.state, new_state)
        not in state_manager.valid_transitions
    )
    with pytest.raises(InvalidTransitionError):
        state_manager.state = new_state


def test_state_manager_get_state_from_packet_raises(state_manager_with_valid):
    with pytest.raises(NotImplementedError):
        state_manager_with_valid._get_state_from_packet(
            BatteryPacket(bytearray([0, 0, 0, 0, 0, 1]))
        )


def test_state_manager_is_transition_valid_raises():
    state_manager = StateManager(initial_state=TEST_POWER_STATES[0])
    with pytest.raises(NotImplementedError):
        state_manager.state = TEST_POWER_STATES[1]


def test_state_manager_log_transition_raises(state_manager_with_valid):
    with pytest.raises(NotImplementedError):
        state_manager_with_valid._log_transition(
            Transition(from_state=TEST_POWER_STATES[0], to_state=TEST_POWER_STATES[1])
        )


def test_state_manager_new_packet_raises(state_manager_with_valid):
    with pytest.raises(NotImplementedError):
        state_manager_with_valid.new_packet(
            BatteryPacket(bytearray([0, 0, 0, 0, 0, 1]))
        )


def test_power_state_manager_transition_time_check(test_power_state_manager):
    """tests time elapsed check returns True for
    _time_since_thresh_crossed_ms > transition_time_check_ms
    """
    state_manager = test_power_state_manager()
    test_data: TestPowerPacketData = [(0, 0), (5, 5), (200, 15), (205, 30)]
    packets = patch_power_packet_data(test_data)
    for packet in packets:
        state_manager.new_packet(packet)
    assert (
        state_manager._time_since_thresh_crossed_ms
        >= state_manager.transition_time_check_ms
    )
    assert state_manager._transition_time_check()


def test_power_state_manager_log_transition(test_logger):
    state_manager = PowerStateManager()
    # check that logged value is the time the initial change took place,
    # not after the time check is complete
    state_manager.time_ms = 3000
    state_manager._time_thresh_crossed_ms = 2900
    state_manager._log_transition(
        Transition(state_manager.state, state_manager.valid_states[1])
    )
    message = "S;2;0-1"
    assert message in test_logger.text


def test_power_state_manager_new_packet_threshold_change():
    """tests that when a new packet is recieved that exceeds a threshold value for a
    state change, the pending state is updated, the state isn't yet changed and
    the time at which the threshold change took place is captured.
    """
    # (power mW, time ms)
    test_time = 15
    test_data: TestPowerPacketData = [(0, 0), (5, 10), (200, test_time)]
    state_manager = PowerStateManager()
    initial_state = state_manager.state
    packets = patch_power_packet_data(test_data)
    for packet in packets:
        # add packet to state manager
        state_manager.new_packet(packet)
    assert state_manager._time_thresh_crossed_ms == test_time
    assert state_manager._pending_state
    assert state_manager._pending_state != initial_state
    assert not state_manager._transition_time_check()


def test_power_state_manager_new_packet_new_state(test_power_state_manager):
    """tests that if a threshold is met for a new state, for longer than the required
    time, state is updated
    """
    test_data: TestPowerPacketData = [(0, 0), (200, 10), (205, 25)]
    state_manager = test_power_state_manager()
    packets = patch_power_packet_data(test_data)
    # set arbitrary threshold for time check for test
    state_manager.transition_time_check_ms = 10
    # check time difference between packets
    assert test_data[-1][1] - test_data[-2][1] >= state_manager.transition_time_check_ms
    initial_state = state_manager.state
    for packet in packets:
        # add packet to state manager
        state_manager.new_packet(packet)
    assert state_manager.state != initial_state


def test_power_state_manager_new_packet_cancels_pending(test_power_state_manager):
    """tests that if a new packet is provided that sits outside of the current state
    threshold, the pending state is cancelled.
    """
    test_data: TestPowerPacketData = [(0, 0), (200, 10), (305, 25)]
    packets = patch_power_packet_data(test_data)
    state_manager = test_power_state_manager()
    for packet in packets:
        # add packet to state manager
        state_manager.new_packet(packet)
    # check that pending state is None
    assert not test_power_state_manager._pending_state


@pytest.mark.parametrize(
    "id, state",
    [
        (0, TEST_BATTERY_STATES[0]),
        (1, TEST_BATTERY_STATES[1]),
        (2, TEST_BATTERY_STATES[2]),
        (3, TEST_BATTERY_STATES[3]),
        (4, None),
    ],
)
def test_battery_state_manager_get_state_from_packet(id, state):
    """tests that the BateryStaterManager._get_state_from_packet
    returns correct state
    """
    state_manager = BatteryStateManager
    state_manager.valid_states = TEST_BATTERY_STATES
    packet = BatteryPacket(bytearray([0, 0, 0, 5, id, (6 + id) % 256]))
    if state:
        assert state_manager()._get_state_from_packet(packet) == state
    else:
        with pytest.raises(StateNotFoundError):
            state_manager()._get_state_from_packet(packet)


@pytest.mark.parametrize(
    "power, state",
    [
        (0, TEST_POWER_STATES[0]),
        (100, TEST_POWER_STATES[0]),
        (250, TEST_POWER_STATES[1]),
        (400, TEST_POWER_STATES[2]),
        (650, TEST_POWER_STATES[3]),
        (845, TEST_POWER_STATES[4]),
        (101, None),
        (350, None),
        (555, None),
        (799, None),
        (901, None),
    ],
)
def test_power_state_manager_get_state_from_packet(
    power, state, test_power_state_manager
):
    """tests that the PowerStateManager._get_state_from_packet
    returns correct state
    """
    state_manager = test_power_state_manager()
    test_data: TestPowerPacketData = [(power, 0)]
    packet = patch_power_packet_data(test_data)[0]
    if state:
        assert state_manager._get_state_from_packet(packet) == state
    else:
        with pytest.raises(StateNotFoundError):
            state_manager._get_state_from_packet(packet)


def test_battery_state_manager_logs_transition_format(test_logger):
    """tests that the correct format is produced for state transitions"""
    state_manager = BatteryStateManager()
    state_manager._log_transition(
        Transition(state_manager.state, TEST_BATTERY_STATES[2])
    )
    message = "B;0;two"
    assert message in test_logger.text


def test_battery_state_manager_updates_with_new_packet():
    """tests that a new packet correctly updates the state manager"""
    state_manager = BatteryStateManager()
    test_state_id = 2
    packet = BatteryPacket(bytearray([0, 0, 0, 5, test_state_id, 8]))
    # update with new packet
    state_manager.new_packet(packet)
    assert state_manager.time_ms == 5
    assert state_manager.current_packet == packet
    assert state_manager.state == state_manager._get_state_by_id(test_state_id)


def test_battery_state_manager_new_packet_raises_state_not_found():
    """tests that the battery state manager raises error if no state found for packet"""
    state_manager = BatteryStateManager()
    packet = BatteryPacket(bytearray([0, 0, 0, 0, 5, 6]))
    assert packet.batteryState == 5
    with pytest.raises(StateNotFoundError):
        state_manager.new_packet(packet)
