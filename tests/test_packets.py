import pytest

from cdp.packets import (
    BatteryPacket,
    PacketCheckSumError,
    PacketLengthError,
    PowerPacket,
    Packet,
)

# Test byte array of power data packet
# voltage = 4v
# time = 3ms
# current = 256 mA
TEST_POWER_DATA = bytearray([0, 0, 0, 4, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 1, 0, 8])


def test_power_packet():
    """tests that the PowerPacket class correctly decodes voltage and current data"""
    packet = PowerPacket(TEST_POWER_DATA)
    assert packet.voltage_v == 3
    assert packet.current_ma == 256
    assert packet.time_ms == 4


@pytest.mark.parametrize(
    "voltage, current, result", [(5, 1, 5), (10, 5, 50), (2, 1, 2)]
)
def test_power_packet_power_mw(voltage, current, result):
    """tests that the PowerPacket class correctly calculates power in milli watts"""
    powerPacket = PowerPacket(TEST_POWER_DATA)
    # override current and voltage
    powerPacket.voltage_v = voltage
    powerPacket.current_ma = current
    assert powerPacket.power_mw == result


@pytest.mark.parametrize(
    "bytes, result",
    [
        (bytearray([0, 0, 0, 1]), 1),
        (bytearray([0, 0, 1, 0]), 256),
        (bytearray([0, 1, 0, 0]), 65536),
        (bytearray([1, 0, 0, 0]), 16777216),
        (bytearray([6, 1, 2, 5]), 100729349),
    ],
)
def test_power_packet_bytes_to_int(bytes, result):
    packet = PowerPacket(TEST_POWER_DATA)
    assert packet._bytes_to_int(bytes) == result


def test_battery_packet():
    """tests that the BatteryPacket class correctly decodes the battery state"""
    test_state_id = 1
    test_data = bytearray([0, 0, 0, 0, test_state_id, 2])
    batteryPacket = BatteryPacket(data=test_data)
    assert batteryPacket.batteryState == test_state_id


@pytest.mark.parametrize(
    "packet_length, valid",
    [(10, False), (15, False), (20, False), (30, False), (0, False), (17, True)],
)
def test_packet_length_check(packet_length, valid):
    """test the packet length check raises on init"""
    test_data = bytearray([i for i in range(packet_length)])
    if not valid:
        with pytest.raises(PacketLengthError):
            PowerPacket(test_data)


def test_packet_length_bytes_raises():
    """test the Packet class forces user to define packet_length"""
    with pytest.raises(NotImplementedError):
        Packet(TEST_POWER_DATA).length_bytes


def test_packet_packet_type_raises():
    """test the Packet class forces user to define packet_type"""
    test_packet = Packet
    test_packet.length_bytes = 17
    with pytest.raises(NotImplementedError):
        test_packet(TEST_POWER_DATA).packet_type


def test_packet_has_error():
    """tests the _has_error method correctly identifies error"""
    good_packet = BatteryPacket(bytearray([0, 0, 0, 1, 1, 3]))
    assert not good_packet._has_error()


def test_packet_error_raises():
    """tests error is raised for packet with bad error check"""
    with pytest.raises(PacketCheckSumError):
        BatteryPacket(bytearray([0, 0, 0, 1, 1, 4]))
