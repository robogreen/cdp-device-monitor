from cdp.packets import BatteryPacket, Packet, PowerPacket
import os
from typing import List

import pytest
from tests.conftest import (
    TEST_DATA_DIR,
    TEST_INTEGRATION_FILE,
    TEST_INTEGRATION_FILE_SIZE_BYTES,
)

from cdp.file_reader import (
    ByteFileReader,
    EndOfFileReached,
    InvalidFileType,
    UnknownPacketType,
)


def test_byte_file_reader_file_not_found():
    """test invalid file path rasies error"""
    with pytest.raises(FileNotFoundError):
        ByteFileReader(os.path.join(TEST_DATA_DIR, "notafile.bin"))


def test_byte_file_reader_invalid_file_type_raises():
    """tests invalid file type extension raises error"""
    with pytest.raises(InvalidFileType):
        ByteFileReader(os.path.join(TEST_DATA_DIR, "empty.txt"))


def test_byte_file_reader_file_size():
    """tests accuracy of file size method"""
    reader = ByteFileReader(TEST_INTEGRATION_FILE)
    assert reader.file_size == TEST_INTEGRATION_FILE_SIZE_BYTES


def test_byte_file_reader_read_next_bytes():
    """tests read_bytes reads bytes and sets end of file"""
    reader = ByteFileReader(TEST_INTEGRATION_FILE)
    # check end of file not set before reading
    assert not reader.end_of_file
    output_bytes: bytearray = []
    for i in range(TEST_INTEGRATION_FILE_SIZE_BYTES):
        result = reader._read_next_bytes(1)
        output_bytes += result
    # check end of file is set
    assert reader.end_of_file
    assert len(output_bytes) == TEST_INTEGRATION_FILE_SIZE_BYTES
    # check that attempting a read after end of file, raises error
    with pytest.raises(EndOfFileReached):
        reader._read_next_bytes(1)


def test_byte_file_reader_read_packet():
    """tests that the ByteFileReader read_packet correclty reads and returns packets"""
    reader = ByteFileReader(TEST_INTEGRATION_FILE)
    packets: List[Packet] = []
    while not reader.end_of_file:
        result = reader.read_packet()
        packets.append(result)
    assert len(packets) == 217

    batteryPacketCount = 0
    powerPacketCount = 0

    for packet in packets:
        if isinstance(packet, PowerPacket):
            powerPacketCount += 1
        elif isinstance(packet, BatteryPacket):
            batteryPacketCount += 1

    assert powerPacketCount == 215
    assert batteryPacketCount == 2


def test_byte_file_reader_raises_unknown():
    """tests that an error is raised for unknown packet types"""
    reader = ByteFileReader(TEST_INTEGRATION_FILE)
    # BatteryPackets no longer valid
    reader.packet_types = [PowerPacket]

    with pytest.raises(UnknownPacketType):
        while not reader.end_of_file:
            reader.read_packet()
