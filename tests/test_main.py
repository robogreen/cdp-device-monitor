"""tests the integration of the application"""

import argparse
from cdp.main import main
import os
from unittest import mock
import logging

import pytest

from tests.conftest import TEST_DATA_DIR

TEST_DIRS = [
    f.path for f in os.scandir(os.path.join(TEST_DATA_DIR, "integration")) if f.is_dir()
]

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "input, output",
    [
        (os.path.join(dir, "input.bin"), os.path.join(dir, "output.txt"))
        for dir in TEST_DIRS
    ],
)
def test_integration_main(input, output, test_logger):
    """tests the integration of the app by taking a series of defined input
    files and comparing them against the expected output
    """
    with open(output, "r") as output_file:
        output_lines = output_file.readlines()

    with mock.patch(
        "argparse.ArgumentParser.parse_args",
        return_value=argparse.Namespace(input_file=input),
    ):
        # run the app
        main()
    print(test_logger.text)
    # obtain log messages
    # remove added "'" and split on new lines, implicitly testing
    # new line ("\n") at end of each output line.
    app_output = test_logger.text.replace("'", "")
    # sperate the lines, retain new line char.
    app_output = [e + "\n" for e in app_output.split("\n") if e]
    assert len(output_lines) == len(app_output)
    # check logged output, line by line
    for app_line, output_line in zip(app_output, output_lines):
        assert app_line == output_line
