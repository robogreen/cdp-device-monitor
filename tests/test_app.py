import pytest

from cdp.packets import PowerPacket
from cdp.app import App
from tests.conftest import TEST_INTEGRATION_FILE
from cdp.file_reader import ByteFileReader, UnknownPacketType


def test_app_smoke():
    """very simple smoke test to ensure app runs"""
    file_reader = ByteFileReader(TEST_INTEGRATION_FILE)
    app = App(file_reader)
    # simply check that no errors are produced
    app.run()


def test_app_raises_unknown_packets(test_logger):
    """tests that unknown packets result in correct log message"""
    file_reader = ByteFileReader(TEST_INTEGRATION_FILE)
    # make BatteryPacket invalid
    file_reader.packet_types = [PowerPacket]
    app = App(file_reader)
    with pytest.raises(UnknownPacketType):
        app.run()
    # check an error is logged.
    assert "ERR:Unknown Packet Type" in test_logger.text
