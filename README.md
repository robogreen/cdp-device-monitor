# CDP Coding Challenge

## About

This application is a Python3 based system for monitoring power level transitions and battery state updates for a given device.

## Design

High level design and application struture, detailed design is documented in doc strings within the application.

The app has been broken down into several objects to represent (but not limitted to):
- Packets
- States
- Transitions
- File Readers
- State Managers
New packets can be added by creating new subclasses of Packet and StateManager and adding them to the appropriate properties of the ByteFileReader and App classes to allow them to interpret the packets accordingly. 

Tests have been implemented at both the unit and integration level, to allow flexibility in design of the objects and to provide confidence in the overall function of the application (including error handling). 


## Usage Instructions

### Installing Dependencies
`poetry install`

### Run the app
`poetry run python cdp/main.py <file>`

## Software Quality

### Code Formatting

Code is formatted with Black.
Flake8 is used for linting, note that, in the .vscode/settings.json file, the max line length for flake8 is set to 88, instead of its default 80 to comply with Black. 

Run formatter:
`poetry run black .`

## Typing

Type hints have been provided throughout the source, check coverage with:
`poetry run mypy .`

### Run Tests
`poetry run test`

#### Adding integration test cases
To add more integration test cases, the test within tests/test_main.py scrapes the tests/data/integration directory for pairs of input.bin and output.txt files within immediate child directories, additional test cases can be appended by creating this pair of files in a new subdirectory without modifying the test. Add a blank line to the end of the output.txt file to replicate the "\n" character that will be appear on the final line output to the console by the application. 

### Run Coverage Report
`poetry run cov`