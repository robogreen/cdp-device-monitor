from cdp.main import LOGGER_FORMAT
import pytest
import sys


def coverage():
    sys.exit(
        pytest.main(
            [
                "--cov-report=term-missing",
                "--cov=cdp",
                "tests/",
                # configure test runner with same log messgae format as app
                "--log-format='{}'".format(LOGGER_FORMAT),
            ]
        )
    )


def test():
    sys.exit(pytest.main(["--log-format='{}'".format(LOGGER_FORMAT)]))


if __name__ == "__main__":
    sys.exit(test())
